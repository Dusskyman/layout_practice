import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:layout_practice/layouts/main_layout.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      allowFontScaling: true,
      builder: () => MaterialApp(
        home: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MainLayout(
      appbar: AppBar(
        title: Text(
          'Some cool text',
          style: TextStyle(fontSize: 20.ssp),
        ),
      ),
      child: Container(
        alignment: Alignment.center,
        color: Colors.green,
        child: IconButton(
          iconSize: ScreenUtil().setWidth(40),
          icon: Icon(Icons.accessibility),
          onPressed: () {},
        ),
      ),
    );
  }
}
