import 'package:flutter/material.dart';

class MainLayout extends StatelessWidget {
  final Widget child;
  final PreferredSizeWidget appbar;

  const MainLayout({
    this.appbar,
    this.child,
  });

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: appbar,
        body: Padding(
          padding: const EdgeInsets.all(10),
          child: Center(
            child: child,
          ),
        ),
      ),
    );
  }
}
